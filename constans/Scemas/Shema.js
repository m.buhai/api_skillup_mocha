const Schema = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "array",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": [],
    "examples": [
        [
            {
                "id": 101,
                "title": "asperiores et eaque",
                "author": "Holly Predovic",
                "rating": 1.25,
                "year_published": 1720
            },
            {
                "id": 102,
                "title": "enim et vitae ut totam",
                "author": "Kaylie Schmidt",
                "rating": 1.85,
                "year_published": 1986
            }
        ]
    ],

    "items": {
        "$id": "#/items",
        "anyOf": [
            {
                "$id": "#/items/anyOf/0",
                "type": "object",
                "title": "The first anyOf schema",
                "description": "An explanation about the purpose of this instance.",
                "default": {},
                "examples": [
                    {
                        "id": 101,
                        "title": "asperiores et eaque",
                        "author": "Holly Predovic",
                        "rating": 1.25,
                        "year_published": 1720
                    }
                ],
                "required": [
                    "id",
                    "title",
                    "author",
                    "rating",
                    "year_published"
                ],
                "properties": {
                    "id": {
                        "$id": "#/items/anyOf/0/properties/id",
                        "type": "number",
                        "title": "The id schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0,
                        "examples": [
                            101
                        ]
                    },
                    "title": {
                        "$id": "#/items/anyOf/0/properties/title",
                        "type": "string",
                        "title": "The title schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "asperiores et eaque"
                        ]
                    },
                    "author": {
                        "$id": "#/items/anyOf/0/properties/author",
                        "type": "string",
                        "title": "The author schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "Holly Predovic"
                        ]
                    },
                    "rating": {
                        "$id": "#/items/anyOf/0/properties/rating",
                        "type": "number",
                        "title": "The rating schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0.0,
                        "examples": [
                            1.25
                        ]
                    },
                    "year_published": {
                        "$id": "#/items/anyOf/0/properties/year_published",
                        "type": "number",
                        "title": "The year_published schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0,
                        "examples": [
                            1720
                        ]
                    }
                },
                "additionalProperties": true
            }
        ]
    }
}

module.exports = {
    Schema
}