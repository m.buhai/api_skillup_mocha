var assert = require('chai').assert
const fetch = require("node-fetch");
const Ajv = require("ajv");
const ajv = new Ajv()
const addContext = require('mochawesome/addContext');
let GlobalObjects = require( "./GlobalObjects")
let schema = require('../constans/Scemas/Shema')

describe('API skill up tests', function () {
    let booksUrl = 'http://localhost:3000/books';
    let shopsUrl = 'http://localhost:3000/shops';
    let username = 'mobidev';
    let password = 'Temp@123';
    let headers = {'Authorization': 'Basic ' + Buffer.from(username + ":" + password).toString('base64')};

    function parseJson(response) {
        if (!response) return
        try {
            return JSON.parse(response);
        } catch (e) {
            console.log(`parseJson() function error: ${e}`)
            assert.fail(`parseJson() function error: ${e}`)
        }
    }

    it('test1', async function () {
        let response = await fetch(booksUrl, {
            method: 'GET',
            headers: {'Authorization': 'Basic bW9iaWRldjpUZW1wQDEyMw=='}
        });
        if (response.status === 200) {
            addContext(this, 'Response status is ' + response.status + '. Request processed successfully.');
        } else {
            addContext(this, 'Response status is ' + response.status + '. The request was not processed successfully.');
            assert.fail(`Response status is: ${response.status}`)
        }
    });

    it('test 2',async function(){
            let response = await fetch(shopsUrl,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = parseJson(answer);
            if(answer.length===20) {
                await addContext(this, 'There is ' + answer.length + ' books from 20');
            } else {
                await addContext(this, 'There is '+answer.length+' books from 20');
                assert.fail(`Counted books are: ${answer.length}`)
            }
        });

    it('test 3',async function(){
            let response = await fetch(booksUrl,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = parseJson(answer);
            let countBooks =[];
            let count=0;
            for (let i = 0; i < answer.length; i++) {
                if(answer[i].author === 'Buster Mueller'){
                    countBooks[count]=answer[i].id; count=count+1;
                }
            }
            if (countBooks.length>0)
            {
                addContext(this, 'There are '+countBooks.length+' books from author');
            }else {
                addContext(this, 'There are '+countBooks.length+' books from author');
                assert.fail(`There is: ${countBooks.length} books`)
            }
        });

    it('test 4', async function(){
            let id=101;
            let response = await fetch(booksUrl+'/'+id,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = parseJson(answer);
            if(answer.id===id){
                addContext(this, 'ID is correct');
            } else {
                addContext(this, 'ID '+answer.id+' is incorrect');
                assert.fail(`incorrect ID is: ${answer.id}`)
            }
        });

    it('test 5',async function(){
            let newBook = {title:'test title', author:'test author', rating: 4.25, year_published: 2020};
            let response = await fetch(booksUrl,{method: 'POST',
                body: JSON.stringify(newBook),
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==', 'Content-Type':'application/json' }});
            if(response.status===201)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = JSON.parse(answer);
            GlobalObjects.bestId = answer.id
            let responseSecond = await fetch('http://localhost:3000/books/'+GlobalObjects.bestId,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});

            let answerSecond = await responseSecond.text();
            answerSecond = JSON.parse(answerSecond);
            delete answerSecond.id;
            if(JSON.stringify(answerSecond) === JSON.stringify(newBook))
            {
                addContext(this, 'books have the same data as sent.');
            } else {
                if(answerSecond.title === undefined){
                    addContext(this, 'Title is empty.');
                    assert.fail(`Title is empty.`)
                } else if(answerSecond.author === undefined){
                    addContext(this, 'Author is empty.');
                    assert.fail(`Author is empty.`)
                } else if(answerSecond.rating === undefined){
                    addContext(this, 'Rating is empty.');
                    assert.fail(`Rating is empty.`)
                } else if(answerSecond.year_published === undefined){
                    addContext(this, 'Year published is empty.');
                    assert.fail(`Year published is empty.`)
                } else if(JSON.stringify(answerSecond.title) !== JSON.stringify(newBook.title)){
                    addContext(this, 'title published is different.');
                    assert.fail(`title is different.`)
                } else if(JSON.stringify(answerSecond.author) !== JSON.stringify(newBook.author)){
                    addContext(this, 'author is different.');
                    assert.fail(`author is different.`)
                } else if(JSON.stringify(answerSecond.rating) !== JSON.stringify(newBook.rating)){
                    addContext(this, 'rating is different.');
                    assert.fail(`rating is different.`)
                } else if(JSON.stringify(answerSecond.year_published) !== JSON.stringify(newBook.year_published)){
                    addContext(this, 'year published is different.');
                    assert.fail(`year published is different.`)
                }

            }

        });

    it('test 6', async function(){
            let updateBook = {title:'test title', author:'test author', rating: 4.25, year_published: 2020};
            let response = await fetch(booksUrl+'/'+GlobalObjects.bestId,{method: 'PUT',
                body: JSON.stringify(updateBook),
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==', 'Content-Type':'application/json' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let responseSecond = await fetch('http://localhost:3000/books/'+GlobalObjects.bestId,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});

            let answer = await responseSecond.text();
            answer = JSON.parse(answer);
            if(JSON.stringify(answer) === JSON.stringify(updateBook))
            {
                addContext(this, 'books have the same data as sent.');
            } else {
                if(answer.title === undefined){
                    addContext(this, 'Title is empty.');
                    assert.fail(`Title is empty.`)
                } else if(answer.author === undefined){
                    addContext(this, 'Author is empty.');
                    assert.fail(`Author is empty.`)
                } else if(answer.rating === undefined){
                    addContext(this, 'Rating is empty.');
                    assert.fail(`Rating is empty.`)
                } else if(answer.year_published === undefined){
                    addContext(this, `Year published is empty.`);
                    assert.fail(`Year published is empty.`)
                } else if(JSON.stringify(answer.title) !== JSON.stringify(updateBook.title)){
                    addContext(this, 'title published is different.');
                    assert.fail(`title is different.`)
                } else if(JSON.stringify(answer.author) !== JSON.stringify(updateBook.author)){
                    addContext(this, `author is different.`);
                    assert.fail(`author is different.`)
                } else if(JSON.stringify(answer.rating) !== JSON.stringify(updateBook.rating)){
                    addContext(this, 'rating is different.');
                    assert.fail(`rating is different.`)
                } else if(JSON.stringify(answer.year_published) !== JSON.stringify(updateBook.year_published)){
                    addContext(this, 'year published is different.');
                    assert.fail(`year published is different.`)
                }}
        });

    it('test 7.1', async function(){
            let response = await fetch(booksUrl+'/'+GlobalObjects.bestId,{method: 'DELETE',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==', 'Content-Type':'application/json' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let responseSecond = await fetch(booksUrl+'/'+GlobalObjects.bestId,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(responseSecond.status===404)
            {
                addContext(this, 'Response status is 404. Request processed successfully.');
            } else {
                addContext(this, 'Response status is'+ responseSecond.status +'. The request was not processed successfully.');
                assert.fail(`Response status is: ${responseSecond.status}`)
            }
            addContext(this, 'Books was deleted');
        });

    it('test 7.2', async function(){
            let response = await fetch(booksUrl,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = JSON.parse(answer);
            for (let i = 0; i < answer.length; i++) {
                for (let a = 0; a < answer.length; a++) {

                    if(answer[i].title === answer[a].title){
                        if(answer[i].id != answer[a].id) {
                            let responce = await fetch('http://localhost:3000/books/'+answer[a].id,{method: 'DELETE',
                                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==', 'Content-Type':'application/json' }});
                            if(response.status===200)
                            {
                                addContext(this, 'Response status is 200. Request processed successfully.');
                                await allureReporter.addStep('Response status is 200. Request processed successfully.')
                            } else {
                                addContext(this, 'Response status is'+ response.status +'. The request was not processed successfully.');
                                assert.fail(`Response status is: ${response.status}`)
                            }
                            let responceSecond = await fetch('http://localhost:3000/books/'+answer[a].id,{method: 'GET',
                                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
                            if(responceSecond.status===404)
                            {
                                addContext(this, 'Response status is 200. Request processed successfully.');
                            } else {
                                addContext(this, 'Response status is'+ responceSecond.status +'. The request was not processed successfully.');
                                assert.fail(`Response status is: ${responceSecond.status}`)
                            }
                            addContext(this, 'Book was deleted');
                            answer[a]='';
                        }
                    }
                }
            }
            let secondResponse = await fetch(booksUrl,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let secondAnswer = await secondResponse.text();
            secondAnswer = JSON.parse(secondAnswer);
            for (let i = 0; i < secondAnswer.length; i++) {
                for (let a = 0; a < secondAnswer.length; a++) {

                    if (secondAnswer[i].title === secondAnswer[a].title) {
                        if (secondAnswer[i].id != secondAnswer[a].id) {
                            addContext(this, 'Book with id:'+ secondAnswer[i].id +'have a copy');
                            assert.fail(`Book with id: ${secondAnswer[i].id} have a copy`)
                        }
                    }
                }
            }
            addContext(this, 'There are no copies of books here');
        });

    it('test 8', async function(){
            let response = await fetch(booksUrl,{method: 'GET',
                headers:{ 'Authorization':'Basic bW9iaWRldjpUZW1wQDEyMw==' }});
            if(response.status===200)
            {
                addContext(this, 'Response status is '+ response.status+'. Request processed successfully.');
            } else {
                addContext(this, 'Response status is '+ response.status+'. The request was not processed successfully.');
                assert.fail(`Response status is: ${response.status}`)
            }
            let answer = await response.text();
            answer = JSON.parse(answer);
            const validate = ajv.compile(schema)
            const valid = validate(answer)
            if (valid === true){
                addContext(this, 'Type of schema is correct.');
            } else {
                addContext(this, 'Type of schema is not correct.');
                assert.fail(`Type of schema is not correct. Valid is: ${valid}`)
            }
        });
});
